import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.hub import load_state_dict_from_url
import torchvision
from functools import partial
from collections import OrderedDict
import math

import os, inspect, sys


def convert_relu_to_swish(model):
    for child_name, child in model.named_children():
        if isinstance(child, nn.ReLU):
            setattr(model, child_name, nn.SiLU(True))
            # setattr(model, child_name, Swish())
        else:
            convert_relu_to_swish(child)


class ResNet18_2D(nn.Module):

    def __init__(self, pretrained=True, num_classes=226, dropout_p=0.5):
        super(ResNet18_2D, self).__init__()
        self.num_classes = num_classes
        model = torchvision.models.resnet18(weights='IMAGENET1K_V1')
        # print(model)
        modules = list(model.children())[:-1]
        # print(modules)
        self.ResNet_2D_18 = nn.Sequential(*modules)
        convert_relu_to_swish(self.ResNet_2D_18)
        self.flatten = nn.Flatten(1)
        self.fc1 = nn.Linear(model.fc.in_features, self.num_classes)
        self.dropout = nn.Dropout(dropout_p, inplace=True)

    def forward(self, x):
        x = self.ResNet_2D_18(x)
        x = self.flatten(x)
        x = self.dropout(x)
        x = self.fc1(x)
        return x


model_ResNet18_2D = ResNet18_2D()
print(model_ResNet18_2D)

dummy_data = torch.rand(1, 3, 128, 128)
print(dummy_data.shape)
torch.onnx.export(model_ResNet18_2D, dummy_data, "/content/ResNet18-2D.onnx")
