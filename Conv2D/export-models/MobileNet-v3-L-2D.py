import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.hub import load_state_dict_from_url
import torchvision
from functools import partial
from collections import OrderedDict
import math

import os, inspect, sys


def convert_relu_to_swish(model):
    for child_name, child in model.named_children():
        if isinstance(child, nn.ReLU):
            setattr(model, child_name, nn.SiLU(True))
            # setattr(model, child_name, Swish())
        else:
            convert_relu_to_swish(child)


class MobileNetV3_Large_2D(nn.Module):

    def __init__(self, pretrained=True, num_classes=226, dropout_p=0.5):
        super(MobileNetV3_Large_2D, self).__init__()
        model = torchvision.models.mobilenet_v3_large(weights='IMAGENET1K_V2',
                                                      progress=False)
        modules = list(model.children())[:-1]
        # print(modules)
        self.MobileNetV3_Large_2D = nn.Sequential(*modules)
        convert_relu_to_swish(self.MobileNetV3_Large_2D)
        self.flatten = nn.Flatten(1)
        self.fc1 = nn.Linear(960, 1280)
        self.hardswish = nn.Hardswish(inplace=True)
        self.dropout = nn.Dropout(p=dropout_p, inplace=True)
        self.fc2 = nn.Linear(1280, num_classes)

    def forward(self, x):
        x = self.MobileNetV3_Large_2D(x)
        x = self.flatten(x)
        x = self.fc1(x)
        x = self.hardswish(x)
        x = self.dropout(x)
        x = self.fc2(x)
        return x


model_MNv3L_2D = MobileNetV3_Large_2D()
print(model_MNv3L_2D)

dummy_data = torch.rand(1, 3, 128, 128)
print(dummy_data.shape)
torch.onnx.export(model_MNv3L_2D, dummy_data, "/content/MobileNetV3-L-2D.onnx")
