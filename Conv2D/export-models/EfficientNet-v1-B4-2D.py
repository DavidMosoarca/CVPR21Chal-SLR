import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.hub import load_state_dict_from_url
import torchvision
from functools import partial
from collections import OrderedDict
import math

import os, inspect, sys


class EfficientNetV1_B4_2D(nn.Module):

    def __init__(self, pretrained=True, num_classes=226, dropout_p=0.4):
        super(EfficientNetV1_B4_2D, self).__init__()
        model = torchvision.models.efficientnet_b4(weights='IMAGENET1K_V1',
                                                   progress=True)
        modules = list(model.children())[:-1]
        self.EfficientNetV1_B4_2D = nn.Sequential(*modules)
        self.flatten = nn.Flatten(1)
        self.dropout = nn.Dropout(p=dropout_p, inplace=True)
        self.fc = nn.Linear(1792, num_classes)

    def forward(self, x):
        x = self.EfficientNetV1_B4_2D(x)
        x = self.flatten(x)
        x = self.dropout(x)
        x = self.fc(x)
        return x


model_ENv1_B4_2D = EfficientNetV1_B4_2D()
print(model_ENv1_B4_2D)

dummy_data = torch.rand(1, 3, 128, 128)
print(dummy_data.shape)
torch.onnx.export(model_ENv1_B4_2D, dummy_data,
                  "/content/EfficientNetV1-B4-2D.onnx")
