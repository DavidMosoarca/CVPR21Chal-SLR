import warnings
from functools import partial
from typing import Any, Callable, List, Optional, Tuple, Type, Union, Sequence

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor
from torch.autograd import Variable
from torch.hub import load_state_dict_from_url
import torchvision
from torchvision.models._utils import _make_divisible, _ovewrite_named_param, handle_legacy_interface
from torchvision.ops.misc import Conv3dNormActivation
from Conv3D.models.SqueezeExcitation_2plus1D import SqueezeExcitation_2plus1D
from torchvision.models.mobilenetv3 import InvertedResidualConfig
from collections import OrderedDict
import os, inspect, sys, math


class InvertedResidual_2plus1D(nn.Module):
    # Implemented as described at section 5 of MobileNetV3 paper
    def __init__(
        self,
        config: InvertedResidualConfig,
        norm_layer: Callable[..., nn.Module],
        se_layer: Callable[..., nn.Module] = partial(
            SqueezeExcitation_2plus1D, scale_activation=nn.Hardsigmoid),
    ):
        super().__init__()
        if not (1 <= config.stride <= 2):
            raise ValueError("illegal stride value")

        self.use_res_connect = config.stride == 1 and config.input_channels == config.out_channels

        layers: List[nn.Module] = []
        activation_layer = nn.Hardswish if config.use_hs else nn.ReLU

        # expand
        if config.expanded_channels != config.input_channels:
            layers.append(
                Conv3dNormActivation(
                    config.input_channels,
                    config.expanded_channels,
                    kernel_size=1,
                    norm_layer=norm_layer,
                    activation_layer=activation_layer,
                ))

        # depthwise
        stride = 1 if config.dilation > 1 else config.stride
        depthwise_mid_channels = (
            config.expanded_channels * 3 * 3 * 3 * config.expanded_channels
        ) // (config.expanded_channels * 3 * 3 + 3 * config.expanded_channels)
        depthwise_mid_channels = _make_divisible(depthwise_mid_channels,
                                                 config.expanded_channels)
        layers.append(
            nn.Sequential(
                Conv3dNormActivation(
                    config.expanded_channels,
                    depthwise_mid_channels,
                    kernel_size=(1, config.kernel, config.kernel),
                    stride=(1, stride, stride),
                    dilation=(1, config.dilation, config.dilation),
                    groups=config.expanded_channels,
                    norm_layer=norm_layer,
                    activation_layer=activation_layer,
                ),
                Conv3dNormActivation(
                    depthwise_mid_channels,
                    config.expanded_channels,
                    kernel_size=(config.kernel, 1, 1),
                    stride=(stride, 1, 1),
                    dilation=(config.dilation, 1, 1),
                    groups=config.expanded_channels,
                    norm_layer=norm_layer,
                    activation_layer=activation_layer,
                ),
            ))
        if config.use_se:
            squeeze_channels = _make_divisible(config.expanded_channels // 4,
                                               8)
            layers.append(se_layer(config.expanded_channels, squeeze_channels))

        # project
        layers.append(
            Conv3dNormActivation(
                config.expanded_channels,
                config.out_channels,
                kernel_size=1,
                norm_layer=norm_layer,
                activation_layer=activation_layer,
            ))

        self.block = nn.Sequential(*layers)
        self.out_channels = config.out_channels
        self._is_cn = config.stride > 1

    def forward(self, input: Tensor) -> Tensor:
        result = self.block(input)
        if self.use_res_connect:
            result += input
        return result


class MobileNetV3_2plus1D(nn.Module):

    def __init__(
        self,
        inverted_residual_config: List[InvertedResidualConfig],
        last_channel: int,
        # stem: Callable[..., nn.Module],
        block: Optional[Callable[..., nn.Module]] = None,
        norm_layer: Optional[Callable[..., nn.Module]] = None,
        num_classes: int = 500,
        dropout: float = 0.2,
        zero_init_residual: bool = False,
        groups: int = 1,
        width_per_group: int = 64,
        replace_stride_with_dilation: Optional[List[bool]] = None,
    ):
        super(MobileNetV3_2plus1D, self).__init__()

        if not inverted_residual_config:
            raise ValueError(
                "The inverted_residual_setting should not be empty")
        elif not (isinstance(inverted_residual_config, Sequence) and all([
                isinstance(s, InvertedResidualConfig)
                for s in inverted_residual_config
        ])):
            raise TypeError(
                "The inverted_residual_setting should be List[InvertedResidualConfig_2plus1D]"
            )

        if block is None:
            block = InvertedResidual_2plus1D

        if norm_layer is None:
            norm_layer = partial(nn.BatchNorm3d, eps=0.001, momentum=0.01)

        layers: List[nn.Module] = []
        # inverted_residual_setting = [bneck_conf(16, 3, 16, 16, False, "RE", 1, 1),]
        firstconv_output_channels = inverted_residual_config[0].input_channels

        # MobileNet2+1dStem()
        # spatial Conv
        stem_input_channels = 3  # RGB
        stem_mid_channels = (
            stem_input_channels * 3 * 3 * 3 * firstconv_output_channels
        ) // (stem_input_channels * 3 * 3 + 3 * firstconv_output_channels) * 2
        layers.append(
            nn.Sequential(
                Conv3dNormActivation(
                    stem_input_channels,
                    stem_mid_channels,
                    kernel_size=(1, 3, 3),
                    stride=(1, 2, 2),
                    padding=(0, 1, 1),
                    norm_layer=norm_layer,
                    activation_layer=nn.Hardswish,
                ),
                Conv3dNormActivation(
                    stem_mid_channels,
                    firstconv_output_channels,
                    kernel_size=(3, 1, 1),
                    stride=(2, 1, 1),
                    padding=(1, 0, 0),
                    norm_layer=norm_layer,
                    activation_layer=nn.Hardswish,
                ),
            ))

        # building inverted residual blocks
        for config in inverted_residual_config:
            layers.append(block(config, norm_layer))

        # building last several layers
        lastconv_input_channels = inverted_residual_config[-1].out_channels
        lastconv_output_channels = 6 * lastconv_input_channels

        layers.append(
            Conv3dNormActivation(
                lastconv_input_channels,
                lastconv_output_channels,
                kernel_size=1,
                norm_layer=norm_layer,
                activation_layer=nn.Hardswish,
            ))

        self.features = nn.Sequential(*layers)
        self.avgpool = nn.AdaptiveAvgPool3d(1)
        self.classifier = nn.Sequential(
            nn.Flatten(1),
            nn.Linear(lastconv_output_channels, last_channel),
            nn.Hardswish(inplace=True),
            nn.Dropout(p=dropout, inplace=True),
            nn.Linear(last_channel, num_classes),
        )

        for m in self.modules():
            if isinstance(m, nn.Conv3d):
                nn.init.kaiming_normal_(m.weight, mode="fan_out")
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, (nn.BatchNorm3d, nn.GroupNorm)):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.zeros_(m.bias)

    def _forward_impl(self, x: Tensor) -> Tensor:
        x = self.features(x)
        x = self.avgpool(x)
        x = self.classifier(x)
        return x

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)


def _mobilenet_v3_2plus1D_config(architecture: str,
                                 width_mult: float = 1.0,
                                 reduced_tail: bool = False,
                                 dilated: bool = False,
                                 **kwargs: Any):
    reduce_divider = 2 if reduced_tail else 1
    dilation = 2 if dilated else 1

    bneck_conf = partial(InvertedResidualConfig, width_mult=width_mult)
    adjust_channels = partial(InvertedResidualConfig.adjust_channels,
                              width_mult=width_mult)

    if architecture == "large":
        inverted_residual_config = [
            bneck_conf(16, 3, 16, 16, False, "RE", 1, 1),
            bneck_conf(16, 3, 64, 24, False, "RE", 2, 1),  # C1
            bneck_conf(24, 3, 72, 24, False, "RE", 1, 1),
            bneck_conf(24, 5, 72, 40, True, "RE", 2, 1),  # C2
            bneck_conf(40, 5, 120, 40, True, "RE", 1, 1),
            bneck_conf(40, 5, 120, 40, True, "RE", 1, 1),
            bneck_conf(40, 3, 240, 80, False, "HS", 2, 1),  # C3
            bneck_conf(80, 3, 200, 80, False, "HS", 1, 1),
            bneck_conf(80, 3, 184, 80, False, "HS", 1, 1),
            bneck_conf(80, 3, 184, 80, False, "HS", 1, 1),
            bneck_conf(80, 3, 480, 112, True, "HS", 1, 1),
            bneck_conf(112, 3, 672, 112, True, "HS", 1, 1),
            bneck_conf(112, 5, 672, 160 // reduce_divider, True, "HS", 2,
                       dilation),  # C4
            bneck_conf(160 // reduce_divider, 5, 960 // reduce_divider,
                       160 // reduce_divider, True, "HS", 1, dilation),
            bneck_conf(160 // reduce_divider, 5, 960 // reduce_divider,
                       160 // reduce_divider, True, "HS", 1, dilation),
        ]
        last_channel = adjust_channels(1280 // reduce_divider)  # C5
    else:
        raise ValueError(f"Unsupported model type {architecture}")

    return inverted_residual_config, last_channel


def mobilenet_v3_L_2plus1D(num_classes = 500, **kwargs: Any) -> MobileNetV3_2plus1D:
    inverted_residual_config, last_channel = _mobilenet_v3_2plus1D_config("large", **kwargs)
    model = MobileNetV3_2plus1D(inverted_residual_config, last_channel, num_classes=num_classes,
                                **kwargs)
    return model
