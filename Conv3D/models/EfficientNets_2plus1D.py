import copy
import math
import warnings
from dataclasses import dataclass
from functools import partial
from typing import Any, Callable, Dict, List, Optional, Sequence, Tuple, Union

import torch
from torch import nn, Tensor
from torchvision.ops import StochasticDepth
from torchvision.ops.misc import Conv3dNormActivation
from Conv3D.models.SqueezeExcitation_2plus1D import SqueezeExcitation_2plus1D
from torchvision.models._utils import _make_divisible, _ovewrite_named_param, handle_legacy_interface
from torchvision.models.efficientnet import _MBConvConfig, MBConvConfig, FusedMBConvConfig


class MBConv_2plus1D(nn.Module):

    def __init__(
        self,
        cnf: MBConvConfig,
        stochastic_depth_prob: float,
        norm_layer: Callable[..., nn.Module],
        se_layer: Callable[..., nn.Module] = SqueezeExcitation_2plus1D,
    ) -> None:
        super().__init__()

        if not (1 <= cnf.stride <= 2):
            raise ValueError("illegal stride value")

        self.use_res_connect = cnf.stride == 1 and cnf.input_channels == cnf.out_channels

        layers: List[nn.Module] = []
        activation_layer = nn.SiLU

        # expand
        expanded_channels = cnf.adjust_channels(cnf.input_channels,
                                                cnf.expand_ratio)
        if expanded_channels != cnf.input_channels:
            layers.append(
                Conv3dNormActivation(
                    cnf.input_channels,
                    expanded_channels,
                    kernel_size=1,
                    norm_layer=norm_layer,
                    activation_layer=activation_layer,
                ))

        # depthwise
        depthwise_mid_channels = (
            expanded_channels * 3 * 3 * 3 * expanded_channels) // (
                expanded_channels * 3 * 3 + 3 * expanded_channels)
        layers.append(
            nn.Sequential(
                Conv3dNormActivation(
                    expanded_channels,
                    depthwise_mid_channels,
                    kernel_size=(1, cnf.kernel, cnf.kernel),
                    stride=(1, cnf.stride, cnf.stride),
                    groups=expanded_channels,
                    norm_layer=norm_layer,
                    activation_layer=activation_layer,
                ),
                Conv3dNormActivation(
                    depthwise_mid_channels,
                    expanded_channels,
                    kernel_size=(cnf.kernel, 1, 1),
                    stride=(cnf.stride, 1, 1),
                    groups=expanded_channels,
                    norm_layer=norm_layer,
                    activation_layer=activation_layer,
                ),
            ))

        # squeeze and excitation
        squeeze_channels = max(1, cnf.input_channels // 4)
        layers.append(
            se_layer(expanded_channels,
                     squeeze_channels,
                     activation=partial(nn.SiLU, inplace=True)))

        # project
        layers.append(
            Conv3dNormActivation(expanded_channels,
                                 cnf.out_channels,
                                 kernel_size=1,
                                 norm_layer=norm_layer,
                                 activation_layer=None))


        self.block = nn.Sequential(*layers)
        self.stochastic_depth = StochasticDepth(stochastic_depth_prob, "row")
        self.out_channels = cnf.out_channels

    def forward(self, input: Tensor) -> Tensor:
        result = self.block(input)
        if self.use_res_connect:
            result = self.stochastic_depth(result)
            result += input
        return result


class FusedMBConv_2plus1D(nn.Module):

    def __init__(
        self,
        config: FusedMBConvConfig,
        stochastic_depth_prob: float,
        norm_layer: Callable[..., nn.Module],
    ) -> None:
        super().__init__()

        if not (1 <= config.stride <= 2):
            raise ValueError("illegal stride value")

        self.use_res_connect = config.stride == 1 and config.input_channels == config.out_channels

        layers: List[nn.Module] = []
        activation_layer = nn.SiLU

        expanded_channels = config.adjust_channels(config.input_channels,
                                                   config.expand_ratio)
        if expanded_channels != config.input_channels:
            # fused expand
            fuse_expand_mid_channels = (
                config.input_channels * 3 * 3 * 3 * expanded_channels) // (
                    config.input_channels * 3 * 3 + 3 * expanded_channels
                ) # is the formula for fuse-expanded-mid-channels proper?
            layers.append(
                nn.Sequential(
                    Conv3dNormActivation(
                        config.input_channels,
                        fuse_expand_mid_channels,
                        kernel_size=(1, config.kernel, config.kernel),
                        stride=(1, config.stride, config.stride),
                        norm_layer=norm_layer,
                        activation_layer=activation_layer,
                    ),
                    Conv3dNormActivation(
                        fuse_expand_mid_channels,
                        expanded_channels,
                        kernel_size=(config.kernel, 1, 1),
                        stride=(config.stride, 1, 1),
                        norm_layer=norm_layer,
                        activation_layer=activation_layer,
                    ),
                ))

            # project
            layers.append(
                Conv3dNormActivation(expanded_channels,
                                     config.out_channels,
                                     kernel_size=1,
                                     norm_layer=norm_layer,
                                     activation_layer=activation_layer))

        else:
            mid_channels = (
                config.input_channels * 3 * 3 * 3 * config.out_channels) // (
                    config.input_channels * 3 * 3 + 3 * config.out_channels
                ) # is the formula for mid-channels proper?
            layers.append(
                nn.Sequential(
                    Conv3dNormActivation(
                        config.input_channels,
                        mid_channels,
                        kernel_size=(1, config.kernel, config.kernel),
                        stride=(1, config.stride, config.stride),
                        norm_layer=norm_layer,
                        activation_layer=activation_layer,
                    ),
                    Conv3dNormActivation(
                        mid_channels,
                        config.out_channels,
                        kernel_size=(config.kernel, 1, 1),
                        stride=(config.stride, 1, 1),
                        norm_layer=norm_layer,
                        activation_layer=activation_layer,
                    ),
                ))

        self.block = nn.Sequential(*layers)
        self.stochastic_depth = StochasticDepth(stochastic_depth_prob, "row")
        self.out_channels = config.out_channels

    def forward(self, input: Tensor) -> Tensor:
        result = self.block(input)
        if self.use_res_connect:
            result = self.stochastic_depth(result)
            result += input
        return result


class EfficientNet_2plus1D(nn.Module):

    def __init__(
        self,
        inverted_residual_config: Sequence[Union[MBConvConfig,
                                                 FusedMBConvConfig]],
        dropout: float,
        stochastic_depth_prob: float = 0.2,
        last_channel: Optional[int] = None,
        num_classes: int = 1000,
        norm_layer: Optional[Callable[..., nn.Module]] = None,
        **kwargs: Any,
    ) -> None:
        super().__init__()

        if not inverted_residual_config:
            raise ValueError(
                "The inverted_residual_config should not be empty")
        elif not (isinstance(inverted_residual_config, Sequence) and all(
            [isinstance(s, _MBConvConfig) for s in inverted_residual_config])):
            raise TypeError(
                "The inverted_residual_config should be List[_MBConvConfig]")

        if "block" in kwargs:
            warnings.warn(
                "The parameter 'block' is deprecated since 0.13 and will be removed 0.15. "
                "Please pass this information on 'MBConvConfig.block' instead."
            )
            if kwargs["block"] is not None:
                for s in inverted_residual_config:
                    if isinstance(s, MBConvConfig):
                        s.block = kwargs["block"]

        if norm_layer is None:
            norm_layer = nn.BatchNorm3d

        layers: List[nn.Module] = []
        stem_input_channels = 3  # RGB
        stem_output_channels = inverted_residual_config[0].input_channels
        stem_mid_channels = (
            stem_input_channels * 3 * 3 * 3 * stem_output_channels) // (
                stem_input_channels * 3 * 3 + 3 * stem_output_channels
            ) * 2  # is the formula for the number of mid-channels proper?
        layers.append(
            nn.Sequential(
                Conv3dNormActivation(
                    stem_input_channels,
                    stem_mid_channels,
                    kernel_size=(1, 3, 3),
                    stride=(1, 2, 2),
                    padding=0,
                    norm_layer=norm_layer,
                    activation_layer=nn.SiLU,
                ),
                Conv3dNormActivation(
                    stem_mid_channels,
                    stem_output_channels,
                    kernel_size=(3, 1, 1),
                    stride=(2, 1, 1),
                    padding=0,
                    norm_layer=norm_layer,
                    activation_layer=nn.SiLU,
                ),
            ))

        total_stage_blocks = sum(config.num_layers
                                 for config in inverted_residual_config)
        stage_block_id = 0
        for config in inverted_residual_config:
            stage: List[nn.Module] = []
            for _ in range(config.num_layers):
                # copy to avoid modifications. shallow copy is enough
                block_config = copy.copy(config)

                # overwrite info if not the first conv in the stage
                if stage:
                    block_config.input_channels = block_config.out_channels
                    block_config.stride = 1

                # adjust stochastic depth probability based on the depth of the stage block
                sd_prob = stochastic_depth_prob * float(
                    stage_block_id) / total_stage_blocks

                stage.append(
                    block_config.block(block_config, sd_prob, norm_layer))
                stage_block_id += 1

            layers.append(nn.Sequential(*stage))

        # building last several layers
        lastconv_input_channels = inverted_residual_config[-1].out_channels
        lastconv_output_channels = last_channel if last_channel is not None else 4 * lastconv_input_channels

        layers.append(
            Conv3dNormActivation(
                lastconv_input_channels,
                lastconv_output_channels,
                kernel_size=1,
                norm_layer=norm_layer,
                activation_layer=nn.SiLU,
            ))

        self.features = nn.Sequential(*layers)
        self.avgpool = nn.AdaptiveAvgPool3d(1)
        self.classifier = nn.Sequential(
            nn.Flatten(1),
            nn.Dropout(p=dropout, inplace=True),
            nn.Linear(lastconv_output_channels, num_classes),
        )

        for m in self.modules():
            if isinstance(m, nn.Conv3d):
                nn.init.kaiming_normal_(m.weight, mode="fan_out")
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            elif isinstance(m, (nn.BatchNorm3d, nn.GroupNorm)):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            elif isinstance(m, nn.Linear):
                init_range = 1.0 / math.sqrt(m.out_features)
                nn.init.uniform_(m.weight, -init_range, init_range)
                nn.init.zeros_(m.bias)

    def _forward_impl(self, x: Tensor) -> Tensor:
        x = self.features(x)
        x = self.avgpool(x)
        x = self.classifier(x)
        return x

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)


def _efficientnet_2plus1D_config(
    arch: str,
    **kwargs: Any,
) -> Tuple[Sequence[Union[MBConvConfig, FusedMBConvConfig]], Optional[int]]:
    inverted_residual_config: Sequence[Union[MBConvConfig, FusedMBConvConfig]]
    if arch.startswith("efficientnet_b"):
        bneck_conf = partial(MBConvConfig,
                             width_mult=kwargs.pop("width_mult"),
                             depth_mult=kwargs.pop("depth_mult"),
                             block=MBConv_2plus1D)
        inverted_residual_config = [
            bneck_conf(1, 3, 1, 32, 16, 1),
            bneck_conf(6, 3, 2, 16, 24, 2),
            bneck_conf(6, 5, 2, 24, 40, 2),
            bneck_conf(6, 3, 2, 40, 80, 3),
            bneck_conf(6, 5, 1, 80, 112, 3),
            bneck_conf(6, 5, 2, 112, 192, 4),
            bneck_conf(6, 3, 1, 192, 320, 1),
        ]
        last_channel = None
    elif arch.startswith("efficientnet_v2_s"):
        bneck_conf = partial(MBConvConfig, block=MBConv_2plus1D)
        fused_bneck_conf = partial(FusedMBConvConfig,
                                   block=FusedMBConv_2plus1D)
        inverted_residual_config = [
            fused_bneck_conf(1, 3, 1, 24, 24, 2),
            fused_bneck_conf(4, 3, 2, 24, 48, 4),
            fused_bneck_conf(4, 3, 2, 48, 64, 4),
            bneck_conf(4, 3, 2, 64, 128, 6),
            bneck_conf(6, 3, 1, 128, 160, 9),
            bneck_conf(6, 3, 2, 160, 256, 15),
        ]
        last_channel = 1280
    else:
        raise ValueError(f"Unsupported model type {arch}")
    return inverted_residual_config, last_channel


def efficientnet_v2_S_2plus1d(num_classes = 1000, **kwargs: Any) -> EfficientNet_2plus1D:
    inverted_residual_config, last_channel = _efficientnet_2plus1D_config(
        "efficientnet_v2_s")
    return EfficientNet_2plus1D(
        inverted_residual_config,
        0.2,
        last_channel,
        num_classes,
        norm_layer=partial(nn.BatchNorm3d, eps=1e-03),
        **kwargs,
    )
