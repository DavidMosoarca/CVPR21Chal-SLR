import torch
import torchvision

import torch.nn as nn
from typing import Any, Callable, List, Optional, Tuple, Type, Union, Sequence
from torch import Tensor


class SqueezeExcitation_2plus1D(nn.Module):

    def __init__(
        self,
        input_channels: int,
        squeeze_channels: int,
        activation: Callable[..., torch.nn.Module] = nn.ReLU,
        scale_activation: Callable[..., torch.nn.Module] = nn.Sigmoid,
    ):
        super().__init__()
        self.avgpool = nn.AdaptiveAvgPool3d(1)
        self.conv1 = nn.Sequential(
            nn.Conv3d(input_channels, squeeze_channels, (1, 1, 1)),
            nn.BatchNorm3d(squeeze_channels),
        )
        self.conv2 = nn.Sequential(
            nn.Conv3d(squeeze_channels, input_channels, (1, 1, 1)),
            nn.BatchNorm3d(input_channels),
        )

        self.activation = activation()
        self.scale_activation = scale_activation()

    def _scale(self, x: Tensor) -> Tensor:
        x = self.avgpool(x)
        x = self.conv1(x)
        x = self.activation(x)
        x = self.conv2(x)
        x = self.scale_activation(x)
        return x

    def forward(self, input: Tensor) -> Tensor:
        scale = self._scale(input)
        return scale * input
